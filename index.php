<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>GNS</title>
    
        <!-- Bootstrap -->
        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/css/style.css" rel="stylesheet">
        <link rel="shortcut icon" href="/img/favicon.png" />
        
        <script src="/js/jquery-1.11.3.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>    
        <script src="/js/js.js"></script>  
    </head>

    <body>

        <!-- Навигация -->
        <div class="navbar navbar-inverse navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#responsiv-menu">
                <span class="sr-only">Открыть навигацию</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
                  
              <a class="navbar-brand" href="/index.php" title="Главная страница">
                <span class="glyphicon glyphicon-home" aria-hidden="true"></span> GNS
              </a>
            </div>
          </div>
        </div>
        <!--/Навигация-->
        
        <!-- Контент -->
        <?
            $_GET['page'] = isset($_GET['page'])? $_GET['page'] : 'select';
            switch ($_GET['page']) {
                 case 'select':
                    include(__DIR__ . '/inc/select.php');
                break;
                case 'new-film':
                    include(__DIR__ . '/inc/new-film.php');
                break;
                case 'see-film':
                    include(__DIR__ . '/inc/see-film.php');
                break;
            }
        ?>

        <!-- /Контент -->

        <!-- Футер -->
        <nav class="navbar navbar-inverse row-fluid footer navbar-fixed-bottom">
            <p class="navbar-text footer text-center" style="width:100%">
                <a class="navbar-link" href="/txt/Тестовое_задание.txt" title="Задание">
                    <span class="glyphicon glyphicon-education" aria-hidden="true"></span> gns-tast-test
                </a>
                <span class="glyphicon glyphicon-copyright-mark" aria-hidden="true">
                </span> 2015
            </p>
        </nav>
        <!-- Футер -->

	</body>
</html>