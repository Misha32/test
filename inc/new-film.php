<div class="container">
    <div class="alert alert-success" role="alert"><center class="films">Добавить новый фильм</center></div>
    <form role="form" style="width:400px; margin: 0 auto;" action="/inc/action-new-film.php" method="post">    

        <div class="required-field-block">
            <input type="text" placeholder="Название фильма" class="form-control" autofocus required name="name">
        </div>
        
        <div class="required-field-block">
            <input type="number" placeholder="Год" class="form-control" required name="year" min="1900" max="2020">
        </div>

        <div class="required-field-block">
            <input type="number" placeholder="Состояние (1 или 0)" class="form-control" min="0" max="1" name="status" required>
        </div>
 
        <button class="btn btn-success">Отправить</button>
    </form>
</div>