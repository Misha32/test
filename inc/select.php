<div class="select">
    <div class="container">
        <div class="row">
            <div class="alert alert-info" role="alert"><center class="films">Films</center></div>
            <center><ul class="ds-btn">
                <li>
                    <a class="btn btn-lg btn-primary" href="?page=see-film">
                        <span> Посмотреть фильмы<br><small>Click here</small></span>
                    </a> 
                </li>
                <li>
                    <a class="btn btn-lg btn-success " href="?page=new-film">
                        <span>Добавить фильм<br><small>Click here</small></span>
                    </a> 
                </li>
            </ul></center>
    	</div>
    </div>
</div>